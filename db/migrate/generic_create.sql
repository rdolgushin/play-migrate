CREATE TABLE Patchlevel (
  version integer NOT NULL,
  status varchar(255) default NULL,
  PRIMARY KEY (version)
);

INSERT INTO Patchlevel (version, status)
  VALUES (0,'Successful');
