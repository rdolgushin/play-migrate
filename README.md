Play migrate module
===================

This is a fork of original module [repository](https://github.com/dcardon/play-migrate)
with some improvements.

Improvements list
-----------------

* [Running db creating will be executed for each db](https://bitbucket.org/rdolgushin/play-migrate/commits/5039a896831a0f04dcaef8225a5a3e469b28aef3)
* [Initial migrations was improved](https://bitbucket.org/rdolgushin/play-migrate/commits/9c14d66e6f64940d2c47ee2accd203710d946de5)

Installing
----------

Modify your `conf/dependencies.yml` as follows:

    :::conf
    # Application dependencies

    require:
        - play
        - migrate -> migrate 1.4

    repositories:
        - rdolgushin:
              type: http
              artifact: http://cdn.bitbucket.org/rdolgushin/play-migrate/downloads/migrate-1.4.zip
              contains:
                  - migrate -> *

And refresh your application dependencies:

    :::bash
    $ play deps --sync


Usage
-----

Basic usage is no different than using the original migrate module.
The only thing that is targeted, this module - you have already
created the necessary database manually (for security reasons).
